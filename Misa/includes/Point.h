#ifndef POINT_H
#define POINT_H

class Point {
public:
    double x, z, y;

    Point();

    Point(double x, double y, double z);
};

#endif //POINT_H
