#include <vector>

#include <string>
#include "Point.h"
#include "Color.h"
#include <GL/glut.h>

#ifndef HISTORIC_CENTER_FACE_H
#define HISTORIC_CENTER_FACE_H

using namespace std;

class Face {
public:
    vector<Point*> points;
    string texCoord;
    Color* color;
    GLuint nro_texture;
    string local;

    Face(int vertices, Point* points[], Color* color);

    Face(int vertices, Point* points[], GLuint nro_texture, string local);

    void setColor(Color* color);
};

#endif //HISTORIC_CENTER_FACE_H
