#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <string>
#include "glm/glm.hpp"
#include "glm/gtx/component_wise.hpp"
#include <fstream>
#include <vector>
#include <sstream>

#include "Point.h"
#include "Camera.h"
#include "Point.h"
#include "Face.h"
#include "Model.h"
#include "SunMoon.h"


#include "tgaload.h"

#define STB_IMAGE_IMPLEMENTATION
#include "ImageImport.h"


#define GLUT_KEY_SHIFT 112
#define COLOR_B  new Color(255,228,196)
#define COLOR_WALL new Color(255,192,203)
#define COLOR_WINDOW new Color(240,128,128)
#define COLOR_PILLARS new Color(253,245,230)
#define COLOR_CINZA new Color(169,169,169)
#define COLOR_TETO new Color(210,105,30)
#define COLOR_GRAMA new Color(0,100,0)
#define COLOR_ESCADA new Color(139,0,0)
#define COLOR_DOOR new Color(230,230,250)
#define COLOR_FLOOR_INTERIOR new Color(139,69,19)
#define COLOR_YELLOW new Color(184,134,11)
#define COLOR_SLIDE new Color(240,255,240)
#define COLOR_DETAILS new Color(244,164,96)
#define COLOR_CHAO_ESCADA new Color(205,133,63)
#define COLOR_BLACK new Color(0,0,0)
#define COLOR_MESA new Color(255,250,205)

// Qtd m�xima de texturas a serem usadas no programa
#define MAX_NO_TEXTURES 2

#define TELHADO 0
#define PAREDE  1

GLuint texture_id[MAX_NO_TEXTURES];


using namespace glm;
using namespace std;


struct Vertex {
    glm::vec3 position;
    glm::vec2 texcoord;
    glm::vec3 normal;
};

struct VertRef {
    VertRef(int v, int vt, int vn) : v(v), vt(vt), vn(vn) { }
    int v, vt, vn;
};

// z = 65, y = 1, x = 12.5
Camera *cam = new Camera(*(new Point(12.5, 1, 12.5)), *(new Point(0, tan(-0.05), -1)), 0.1, 0.003);
Model building;

static unsigned int redisplay_interval = 1000 / 60;

float door_angle1 = 0.0f, door_angle2 = 0.0f;

bool keyPressed[256], specialKeyPressed[256];

static int axisY[3] = {0, 1, 0}, axisZ[3] = {0, 0, 1}, axisX[3] = {1, 0, 0};

//paredes exteriores
void exteriorWalls(){
    //paredes externas menores
    //frente
    building.addRectFace(new Point(-10, 0, -5), new Point(-10, 8, -5), new Point(0, 8, -5),
                         new Point(0, 0, -5),
                         COLOR_WALL);
    building.addRectFace(new Point(24, 0, -5), new Point(24, 8, -5), new Point(34, 8, -5),
                         new Point(34, 0, -5),
                         COLOR_WALL);
    //tras
    building.addRectFace(new Point(-10, 0, -15), new Point(-10, 8, -15), new Point(0, 8, -15),
                         new Point(0, 0, -15),
                         COLOR_WALL);
    building.addRectFace(new Point(24, 0, -15), new Point(24, 8, -15), new Point(34, 8, -15),
                         new Point(34, 0, -15),
                         COLOR_WALL);
    //laterais
    building.addRectFace(new Point(-10, 0, -5), new Point(-10, 8, -5), new Point(-10, 8, -15),
                         new Point(-10, 0, -15),
                       COLOR_WALL);
    building.addRectFace(new Point(-10, 8, -5), new Point(-10, 8, -10), new Point(-10, 8, -15),
                         new Point(-10, 10, -10),
                         COLOR_WALL);

    building.addRectFace(new Point(34, 0, -5), new Point(34, 8, -5), new Point(34, 8, -15),
                         new Point(34, 0, -15),
                         COLOR_WALL);
    //triangulo
    building.addRectFace(new Point(34, 8, -5), new Point(34, 8, -10), new Point(34, 8, -15),
                        new Point(34, 10, -10),
                        COLOR_WALL);

    //paredes externas maiores
    building.addRectFace(new Point(0, 0, -3), new Point(0, 14, -3), new Point(24, 14, -3),
                         new Point(24, 0, -3),
                         COLOR_WALL);
    building.addRectFace(new Point(0, 0, -17), new Point(0, 14, -17), new Point(10, 14, -17),
                         new Point(10, 0, -17),
                         COLOR_WALL);
    building.addRectFace(new Point(10, 5, -17), new Point(10, 14, -17), new Point(14, 14, -17),
                         new Point(14, 5, -17),
                         COLOR_WALL);
    building.addRectFace(new Point(14, 0, -17), new Point(14, 14, -17), new Point(24, 14, -17),
                         new Point(24, 0, -17),
                         COLOR_WALL);
    //laterais menores
    building.addRectFace(new Point(0, 0, -3), new Point(0, 14, -3), new Point(0, 14, -5),
                         new Point(0, 0, -5),
                         COLOR_WALL);

    building.addRectFace(new Point(24, 0, -3), new Point(24, 14, -3), new Point(24, 14, -5),
                         new Point(24, 0, -5),
                         COLOR_WALL);
    building.addRectFace(new Point(0, 0, -15), new Point(0, 14, -15), new Point(0, 14, -17),
                         new Point(0, 0, -17),
                         COLOR_WALL);

    building.addRectFace(new Point(24, 0, -15), new Point(24, 14, -15), new Point(24, 14, -17),
                         new Point(24, 0, -17),
                         COLOR_WALL);
    //laterais de cima menores
    building.addRectFace(new Point(0, 8, -5), new Point(0, 14, -5), new Point(0, 14, -15),
                         new Point(0, 8, -15),
                         COLOR_WALL);

    building.addRectFace(new Point(24, 8, -5), new Point(24, 14, -5), new Point(24, 14, -15),
                         new Point(24, 8, -15),
                         COLOR_WALL);
}

void outsideWindows(){
    //janelas lado esquerdo
    building.addRectFace(new Point(-9, 2, -4.9), new Point(-9,6, -4.9), new Point(-7, 6, -4.9),
                         new Point(-7, 2, -4.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-6, 0, -4.9), new Point(-6, 6, -4.9), new Point(-4, 6, -4.9),
                         new Point(-4, 0, -4.9),
                         COLOR_WINDOW);
    //degraus
    building.addCube(new Point(-6.5, 0, -4.9), 3, 0.3, 2, COLOR_ESCADA);
    building.addCube(new Point(-6.2, 0.3, -4.9), 2.5, 0.3, 1.5, COLOR_ESCADA);
    building.addCube(new Point(-6.1, 0.6, -4.9), 2.2, 0.3, 1, COLOR_ESCADA);


    building.addRectFace(new Point(-3, 2, -4.9), new Point(-3, 6, -4.9), new Point(-1, 6, -4.9),
                         new Point(-1, 2, -4.9),
                         COLOR_WINDOW);

    //janelas lado direito
    building.addRectFace(new Point(25, 2, -4.9), new Point(25,6, -4.9), new Point(27, 6, -4.9),
                         new Point(27, 2, -4.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(28, 2, -4.9), new Point(28, 6, -4.9), new Point(30, 6, -4.9),
                         new Point(30, 2, -4.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(31, 2, -4.9), new Point(31, 6, -4.9), new Point(33, 6, -4.9),
                         new Point(33, 2, -4.9),
                         COLOR_WINDOW);
    //janelas lado esquerdo tras
    building.addRectFace(new Point(-9, 2, -15.1), new Point(-9,6, -15.1), new Point(-7, 6, -15.1),
                         new Point(-7, 2, -15.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-6, 2, -15.1), new Point(-6, 6, -15.1), new Point(-4, 6, -15.1),
                         new Point(-4, 2, -15.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-3, 2, -15.1), new Point(-3, 6, -15.1), new Point(-1, 6, -15.1),
                         new Point(-1, 2, -15.1),
                         COLOR_WINDOW);
    //janelas lado direito tras
    building.addRectFace(new Point(25, 2, -15.1), new Point(25,6, -15.1), new Point(27, 6, -15.1),
                         new Point(27, 2, -15.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(28, 2, -15.1), new Point(28, 6, -15.1), new Point(30, 6, -15.1),
                         new Point(30, 2, -15.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(31, 2, -15.1), new Point(31, 6, -15.1), new Point(33, 6, -15.1),
                         new Point(33, 2, -15.1),
                         COLOR_WINDOW);
    //janelas lateral esquerdo
    building.addRectFace(new Point(-10.1, 2, -6), new Point(-10.1,6, -6), new Point(-10.1, 6, -8),
                         new Point(-10.1, 2, -8),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-10.1, 2, -9), new Point(-10.1, 6, -9), new Point(-10.1, 6, -11),
                         new Point(-10.1, 2, -11),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-10.1, 2, -12), new Point(-10.1, 6, -12), new Point(-10.1, 6, -14),
                         new Point(-10.1, 2, -14),
                         COLOR_WINDOW);

    //janelas lateral direita
    building.addRectFace(new Point(34.1, 2, -6), new Point(34.1,6, -6), new Point(34.1, 6, -8),
                         new Point(34.1, 2, -8),
                         COLOR_WINDOW);
    //porta
    building.addRectFace(new Point(34.1, 0, -9), new Point(34.1, 6, -9), new Point(34.1, 6, -11),
                         new Point(34.1, 0, -11),
                         COLOR_WINDOW);
    //rampa
    building.addRectFace(new Point(37, 0, -9), new Point(34.1, 1, -9), new Point(34.1, 1, -11),
                         new Point(37, 0, -11),
                         COLOR_WINDOW);
    //escada
    building.addCube(new Point(34.1, 0, -13), 1, 0.7, 5, COLOR_ESCADA);
    building.addCube(new Point(34.1, 0, -13.3), 1.7, 0.4, 5.6, COLOR_ESCADA);
    building.addCube(new Point(34.1, 0, -13.6), 2.2, 0.3, 6.2, COLOR_ESCADA);

    //janela
    building.addRectFace(new Point(34.1, 2, -12), new Point(34.1, 6, -12), new Point(34.1, 6, -14),
                         new Point(34.1, 2, -14),
                         COLOR_WINDOW);

    //janelas frente
    building.addRectFace(new Point(2, 2, -2.9), new Point(2,6, -2.9), new Point(6, 6, -2.9),
                         new Point(6, 2, -2.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(10, 2, -2.9), new Point(10, 6, -2.9), new Point(14, 6, -2.9),
                         new Point(14, 2, -2.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(18, 2, -2.9), new Point(18, 6, -2.9), new Point(22, 6, -2.9),
                         new Point(22, 2, -2.9),
                         COLOR_WINDOW);
    //porta lateral  pequena
    building.addRectFace(new Point(24.1, 0, -3.5), new Point(24.1, 4, -3.5), new Point(24.1, 4, -4.5),
                         new Point(24.1, 0, -4.5),
                        COLOR_WINDOW);
    //RAMPA
     building.addRectFace(new Point(27, 0, -3.5), new Point(24, 1, -3.5), new Point(24, 1, -4.5),
                        new Point(27, 0, -4.5),
                         COLOR_WINDOW);
    //escada
    building.addCube(new Point(24.1, 0, -4.6), 1, 0.6, 1.2, COLOR_ESCADA);
    building.addCube(new Point(24.1, 0, -4.7), 1.7, 0.4, 1.4, COLOR_ESCADA);
    building.addCube(new Point(24.1, 0, -4.8), 2.2, 0.3, 1.6, COLOR_ESCADA);

}

//patio
void  courtyard(){
    //base da estatua
    building.addCube(new Point(10.5, 0, 25), 3, 1, 2, COLOR_CINZA);
    building.addCylinder(new Point(12.2, 0, 26), 7, 0.01, 0.01, -90.0f, axisX, COLOR_GRAMA);


    //lamparinas
    building.addCube(new Point(0, 3, 20), 0.3, 0.9, 0.3, COLOR_PILLARS);
    building.addCylinder(new Point(0.2, 0, 20), 0.1, 0.1, 3, -90.0f, axisX, COLOR_CINZA);
    building.addCube(new Point(0, 0, 20), 0.3, 0.4, 0.3, COLOR_CINZA);

    building.addCube(new Point(24, 3, 20), 0.3, 0.9, 0.3, COLOR_PILLARS);
    building.addCylinder(new Point( 24, 0, 20), 0.1, 0.1, 3, -90.0f, axisX, COLOR_CINZA);
    building.addCube(new Point(24, 0, 20), 0.3, 0.4, 0.3, COLOR_CINZA);

    building.addCube(new Point(0, 3, 30), 0.3, 0.9, 0.3, COLOR_PILLARS);
    building.addCylinder(new Point(0, 0, 30), 0.1, 0.1, 3, -90.0f, axisX, COLOR_CINZA);
    building.addCube(new Point(0, 0, 30), 0.3, 0.4, 0.3, COLOR_CINZA);

    building.addCube(new Point(24, 3, 30), 0.3, 0.9, 0.3, COLOR_PILLARS);
    building.addCylinder(new Point( 24, 0, 30), 0.1, 0.1, 3, -90.0f, axisX, COLOR_CINZA);
    building.addCube(new Point(24, 0, 30), 0.3, 0.4, 0.3, COLOR_CINZA);

}

//colunas
void hill(){

    //colunas menores extremos
    building.addCube(new Point(-10, 0, -5), 0.1, 8 , 0.15, COLOR_PILLARS);
    building.addCube(new Point(-10, 0, -15), 0.1, 8 , -0.15, COLOR_PILLARS);
    building.addCube(new Point(34, 0, -15), 0.1, 8 , -0.15, COLOR_PILLARS);
    building.addCube(new Point(34, 0, -5), 0.1, 8 , 0.15, COLOR_PILLARS);

    //colunas cetrais menores
    building.addCube(new Point(-0.1, 0, -5), 0.1, 8 , 0.15, COLOR_PILLARS);
    building.addCube(new Point(24, 0, -5), 0.1, 8 , 0.15, COLOR_PILLARS);
    building.addCube(new Point(-0.1, 0, -15), 0.1, 8 , -0.15, COLOR_PILLARS);
    building.addCube(new Point(24, 0, -15), 0.1, 8 , -0.15, COLOR_PILLARS);


    //colunas  centrais exteriores
    building.addCube(new Point(-0.1, 0, -3), 0.1, 14 , 0.15, COLOR_PILLARS);
    building.addCube(new Point(-0.1, 0, -17), 0.1, 14 , -0.15, COLOR_PILLARS);
    building.addCube(new Point(24, 0, -17), 0.1, 14, -0.15, COLOR_PILLARS);
    building.addCube(new Point(24, 0, -3), 0.1, 14, 0.15, COLOR_PILLARS);

    building.addCube(new Point(8, 0, -3), 0.1, 14, 0.15, COLOR_PILLARS);
    building.addCube(new Point(16, 0, -3), 0.1, 14, 0.15, COLOR_PILLARS);
    building.addCube(new Point(8, 0, -17), 0.1, 14, -0.15, COLOR_PILLARS);
    building.addCube(new Point(16, 0, -17), 0.1, 14, -0.15, COLOR_PILLARS);

    //colunas horizontais
    building.addCube(new Point(0, 8, -2.9), 24, 0.1, 1, COLOR_PILLARS);
    building.addCube(new Point(0, 8, -17.1), 24, 0.1, -1, COLOR_PILLARS);

}

//teto
void ceiling(){
    //TETO menor direita
   building.addRectFace(new Point(-10, 10, -10), new Point(-10, 8, -5), new Point(0, 8, -5),
                         new Point(0, 10, -10),
                         COLOR_TETO);
   building.addRectFace(new Point(0, 10, -10), new Point(0, 8, -15), new Point(-10, 8, -15),
                         new Point(-10, 10, -10),
                         COLOR_TETO);

   //TETO menor esquerda
   building.addRectFace(new Point(24, 10, -10), new Point(24, 8, -5), new Point(34, 8, -5),
                         new Point(34, 10, -10),
                         COLOR_TETO);
   building.addRectFace(new Point(34, 10, -10), new Point(34, 8, -15), new Point(24, 8, -15),
                         new Point(24, 10, -10),
                         COLOR_TETO);

    //teto maior
    // building.addRectFace(new Point(0, 14, -3), new Point(10,14, -3), new Point(24, 14, -3),
    //                      new Point(12, 18, -8),
    //                      COLOR_TETO);

    building.addRectFace(new Point(34, 8, -5), new Point(34, 8, -10), new Point(34, 8, -15),
                        new Point(34, 10, -10),texture_id[TELHADO], "telhado");

    building.addRectFace(new Point(24, 14, -17), new Point(10,14, -17), new Point(0, 14, -17),
                         new Point(12, 18, -12),
                         COLOR_TETO);
    building.addRectFace(new Point(12, 18, -12), new Point(0,14, -17), new Point(0, 14, -3),
                         new Point(12, 18, -8),
                         COLOR_TETO);
    building.addRectFace(new Point(12, 18, -8), new Point(24,14, -3), new Point(24, 14, -17),
                         new Point(12, 18, -12),
                         COLOR_TETO);
}

//janelas de cima
void specialWindows(){
    //janelas de cima da frente
    building.addRectFace(new Point(2, 9, -2.9), new Point(2, 13, -2.9), new Point(6, 13, -2.9),
                         new Point(6, 9, -2.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(10, 9, -2.9), new Point(10, 13, -2.9), new Point(14, 13, -2.9),
                         new Point(14, 9, -2.9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(18, 9, -2.9), new Point(18, 13, -2.9), new Point(22, 13, -2.9),
                         new Point(22, 9, -2.9),
                         COLOR_WINDOW);


    //janelas de cima de tras
    building.addRectFace(new Point(2, 9, -17.1), new Point(2, 13, -17.1), new Point(6, 13, -17.1),
                         new Point(6, 9, -17.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(10, 9, -17.1), new Point(10, 13, -17.1), new Point(14, 13, -17.1),
                         new Point(14, 9, -17.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(18, 9, -17.1), new Point(18, 13, -17.1), new Point(22, 13, -17.1),
                         new Point(22, 9, -17.1),
                         COLOR_WINDOW);

    //janelas de tras de baixo
    building.addRectFace(new Point(18, 2, -17.1), new Point(18, 6, -17.1), new Point(22, 6, -17.1),
                         new Point(22, 2, -17.1),
                         COLOR_WINDOW);
    building.addRectFace(new Point(2, 2, -17.1), new Point(2, 6, -17.1), new Point(6, 6, -17.1),
                         new Point(6, 2, -17.1),
                         COLOR_WINDOW);

    //janelaqs laterais esquerdas
    building.addRectFace(new Point(-0.1, 10, -4), new Point(-0.1, 13, -4), new Point(-0.1, 13, -6),
                         new Point(-0.1, 10, -6),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-0.1, 10, -7), new Point(-0.1, 13, -7), new Point(-0.1, 13, -9),
                         new Point(-0.1, 10, -9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-0.1, 10, -11), new Point(-0.1, 13, -11), new Point(-0.1, 13, -13),
                         new Point(-0.1, 10, -13),
                         COLOR_WINDOW);
    building.addRectFace(new Point(-0.1, 10, -14), new Point(-0.1, 13, -14), new Point(-0.1, 13, -16),
                         new Point(-0.1, 10, -16),
                         COLOR_WINDOW);

    //janelaqs laterais direitas
    building.addRectFace(new Point(24.1, 10, -4), new Point(24.1, 13, -4), new Point(24.1, 13, -6),
                         new Point(24.1, 10, -6),
                         COLOR_WINDOW);
    building.addRectFace(new Point(24.1, 10, -7), new Point(24.1, 13, -7), new Point(24.1, 13, -9),
                         new Point(24.1, 10, -9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(24.1, 10, -11), new Point(24.1, 13, -11), new Point(24.1, 13, -13),
                         new Point(24.1, 10, -13),
                         COLOR_WINDOW);
    building.addRectFace(new Point(24.1, 10, -14), new Point(24.1, 13, -14), new Point(24.1, 13, -16),
                         new Point(24.1, 10, -16),
                         COLOR_WINDOW);

}

//janelas de cima de dentro
void interiorWindows(){
     //janelas de cima da frente
    building.addRectFace(new Point(10, 9, -3.2), new Point(10, 13, -3.2), new Point(14, 13, -3.2),
                         new Point(14, 9, -3.2),
                         COLOR_WINDOW);
    building.addRectFace(new Point(18, 9, -3.2), new Point(18, 13, -3.2), new Point(22, 13, -3.2),
                         new Point(22, 9, -3.2),
                         COLOR_WINDOW);

    //janela de tras
    building.addRectFace(new Point(2, 9, -16.8), new Point(2, 13, -16.8), new Point(6, 13, -16.8),
                         new Point(6, 9, -16.8),
                         COLOR_WINDOW);
    building.addRectFace(new Point(18, 9, -16.8), new Point(18, 13, -16.8), new Point(22, 13, -16.8),
                         new Point(22, 9, -16.8),
                         COLOR_WINDOW);

    //janelaqs laterais esquerdas
    building.addRectFace(new Point(0.15, 10, -4), new Point(0.15, 13, -4), new Point(0.15, 13, -6),
                         new Point(0.15, 10, -6),
                         COLOR_WINDOW);
    building.addRectFace(new Point(0.15, 10, -7), new Point(0.15, 13, -7), new Point(0.15, 13, -9),
                         new Point(0.15, 10, -9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(0.15, 10, -11), new Point(0.15, 13, -11), new Point(0.15, 13, -13),
                         new Point(0.15, 10, -13),
                         COLOR_WINDOW);
    building.addRectFace(new Point(0.15, 10, -14), new Point(0.15, 13, -14), new Point(0.15, 13, -16),
                         new Point(0.15, 10, -16),
                         COLOR_WINDOW);

    //janelaqs laterais direitas
    building.addRectFace(new Point(23.8, 10, -7), new Point(23.8, 13, -7), new Point(23.8, 13, -9),
                         new Point(23.8, 10, -9),
                         COLOR_WINDOW);
    building.addRectFace(new Point(23.8, 10, -11), new Point(23.8, 13, -11), new Point(23.8, 13, -13),
                         new Point(23.8, 10, -13),
                         COLOR_WINDOW);
    building.addRectFace(new Point(23.8, 10, -14), new Point(23.8, 13, -14), new Point(23.8, 13, -16),
                         new Point(23.8, 10, -16),
                         COLOR_WINDOW);


    //SLIDE
    building.addRectFace(new Point(8, 9, -16.9), new Point(8, 13, -16.9), new Point(16, 13, -16.9),
                         new Point(16, 9, -16.9),
                         COLOR_SLIDE);
    building.addRectFace(new Point(8, 9, -16.8), new Point(8, 9.25, -16.8), new Point(16, 9.25, -16.8),
                         new Point(16, 9, -16.8),
                         COLOR_BLACK);
    building.addRectFace(new Point(8, 13, -16.8), new Point(8, 13.25, -16.8), new Point(16, 13.25, -16.8),
                         new Point(16, 13, -16.8),
                         COLOR_BLACK);

}

//sacadas
void balcony(){
    //sacada da frente
    building.addCube(new Point(8, 8, -3), 0.5, 1, 1, COLOR_PILLARS);
    building.addCube(new Point(15.5, 8, -3), 0.5, 1, 1, COLOR_PILLARS);
    building.addCube(new Point(8, 9, -2), 8, 0.2, 0.1, COLOR_PILLARS);

    building.addCube(new Point(9, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(10, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(11, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(12, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(13, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(14, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(15, 8, -2), 0.3, 1, 0.1, COLOR_PILLARS);

    //sacada de tras
    building.addCube(new Point(8, 8, -17), 0.5, 1, -1, COLOR_PILLARS);
    building.addCube(new Point(15.5, 8, -17), 0.5, 1, -1, COLOR_PILLARS);
    building.addCube(new Point(8, 9, -18), 8, 0.2, 0.1, COLOR_PILLARS);

    building.addCube(new Point(9, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(10, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(11, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(12, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(13, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(14, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);
    building.addCube(new Point(15, 8, -18), 0.3, 1, 0.1, COLOR_PILLARS);


    //sacada da porta esquerda
    building.addCylinder(new Point(8.7, 1, -14.5), 0.2, 0.2, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(8.7, 2, -14.5), 0.3, 0.3, 0.2, -90.0f, axisX, COLOR_DETAILS);

    building.addCylinder(new Point(8.7, 1, -15), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(8.7, 1, -15.5), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(8.7, 1, -16), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(8.7, 1, -16.5), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);

    building.addCube(new Point(8.7, 2, -14.3), 0.2, 0.2, -2.69, COLOR_DETAILS);
    building.addCube(new Point(8.7, 1, -14.5), 0.2, 0.2, -2.49, COLOR_DETAILS);

    //sacada da porta direita
    building.addCylinder(new Point(15.3, 1, -14.5), 0.2, 0.2, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(15.3, 2 ,-14.5), 0.3, 0.3, 0.2, -90.0f, axisX, COLOR_DETAILS);

    building.addCylinder(new Point(15.3, 1, -15), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(15.3, 1, -15.5), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(15.3, 1, -16), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(15.3, 1, -16.5), 0.1, 0.1, 1, -90.0f, axisX, COLOR_DETAILS);

    building.addCube(new Point(15.3, 2, -14.3), 0.2, 0.2, -2.69, COLOR_DETAILS);
    building.addCube(new Point(15.3, 1, -14.5), 0.2, 0.2, -2.49, COLOR_DETAILS);

    building.addCube(new Point(8, 8, -3), 0.5, 1, 1, COLOR_PILLARS);

    //corrimão
    building.addCube(new Point(21, 8, -8), -0.2, 1, -0.2, COLOR_ESCADA);
    building.addCube(new Point(21, 8, -7), -0.2, 1, -0.2, COLOR_ESCADA);
    building.addCube(new Point(21, 8, -6), -0.2, 1, -0.2, COLOR_ESCADA);
    building.addCube(new Point(21, 8, -5), -0.2, 1, -0.2, COLOR_ESCADA);
    building.addCube(new Point(21, 8, -4), -0.2, 1, -0.2, COLOR_ESCADA);
    building.addCube(new Point(21, 8, -2.7), -0.2, 1, -0.2, COLOR_ESCADA);

    building.addCube(new Point(21, 8, -8), 0.2, 0.2, 4.9, COLOR_ESCADA);
    building.addCube(new Point(21, 9, -8.3), 0.2, 0.2, 5.2, COLOR_ESCADA);

}

void walls(){
    //terreo
    //chão
    building.addCube(new Point(-9.9, 0, -5.1), 43.8, 1, -9.8, COLOR_FLOOR_INTERIOR);
    building.addCube(new Point(0.1, 0, -3.1), 23.8, 1, -2, COLOR_FLOOR_INTERIOR);
    building.addCube(new Point(0.1, 0, -14.5), 9, 1, -2.49, COLOR_FLOOR_INTERIOR);
    building.addCube(new Point(23.8, 0, -14.5), -9, 1, -2.49, COLOR_FLOOR_INTERIOR);

    //chao de cima
    building.addRectFace(new Point(0, 8, -8), new Point(0, 8, -15), new Point(23.9,  8, -15),
                         new Point(23.9, 8, -8),
                         COLOR_FLOOR_INTERIOR);
    building.addRectFace(new Point(0, 8, -3), new Point(0, 8, -8), new Point(21,  8, -8),
                         new Point(21, 8, -3),
                         COLOR_FLOOR_INTERIOR);


    //Paredes internas lateral direita
    building.addRectFace(new Point(-9.9, 0, -5.1), new Point(-9.9, 8, -5.1), new Point(-9.9,  8, -14.9),
                         new Point(-9.9, 0, -14.9),
                         COLOR_PILLARS);
    building.addRectFace(new Point(-9.9, 0, -14.9), new Point(-9.9, 8, -14.9), new Point(0.1, 8, -14.9),
                         new Point(0.1, 0, -14.9),
                         COLOR_PILLARS);
    building.addRectFace(new Point(-9.9, 0, -5.1), new Point(-9.9, 8, -5.1), new Point(0.1, 8, -5.1),
                         new Point(0.1, 0, -5.1),
                         COLOR_PILLARS);


    //paredes internas lateral esquerda
    building.addRectFace(new Point(23.8, 0, -5.1), new Point(23.8, 8, -5.1), new Point(33.9, 8, -5.1),
                         new Point(33.9, 0, -5.1),
                         COLOR_PILLARS);
    building.addRectFace(new Point(33.9, 0, -5.1), new Point(33.9, 8, -5.1), new Point(33.9, 8, -14.9),
                         new Point(33.9, 0, -14.9),
                         COLOR_PILLARS);
    building.addRectFace(new Point(33.9, 0, -14.9), new Point(33.9, 8, -14.9), new Point(23.8, 8, -14.9),
                         new Point(23.8, 0, -14.9),
                         COLOR_PILLARS);

   //laterais de cima menores
    building.addRectFace(new Point(0.1, 8, -5), new Point(0.1, 14, -5), new Point(0.1, 14, -15),
                         new Point(0.1, 8, -15),
                         COLOR_PILLARS);

    building.addRectFace(new Point(23.9, 8, -5), new Point(23.9, 14, -5), new Point(23.9, 14, -15),
                         new Point(23.9, 8, -15),
                         COLOR_PILLARS);
    //paredes da frente
    building.addRectFace(new Point(0, 0, -3.1), new Point(0, 14 ,-3.1),new Point(24, 14, -3.1),
                         new Point(24, 0., -3.1),
                         COLOR_PILLARS);

    //paredes de tras
    building.addRectFace(new Point(0, 0, -16.99), new Point(0, 14, -16.99),new Point(10, 14, -16.99),
                         new Point(10, 0., -16.99),
                         COLOR_PILLARS);

    building.addRectFace(new Point(10, 5, -16.99), new Point(10, 14, -16.99),new Point(16, 14, -16.99),
                         new Point(16, 5, -16.99),
                         COLOR_PILLARS);
    building.addRectFace(new Point(14, 0, -16.99), new Point(14, 14, -16.99),new Point(24, 14, -16.99),
                         new Point(24, 0., -16.99),
                         COLOR_PILLARS);

    //DIVISORIAS
    building.addRectFace(new Point(-5, 0, -5.1), new Point(-5, 8, -5.1), new Point(-5, 8, -10),
                         new Point(-5, 0, -10),
                         COLOR_PILLARS);
    building.addRectFace(new Point(29, 0, -5.1), new Point(29, 8, -5.1), new Point(29, 8, -10),
                         new Point(29, 0, -10),
                         COLOR_PILLARS);
    building.addRectFace(new Point(-5, 6, -10), new Point(-5, 8, -10), new Point(-5, 8, -12),
                         new Point(-5, 6, -12),
                         COLOR_PILLARS);
    building.addRectFace(new Point(29, 6, -10), new Point(29, 8, -10), new Point(29, 8, -12),
                         new Point(29, 6, -12),
                         COLOR_PILLARS);

    building.addRectFace(new Point(29, 0, -10), new Point(29, 6, -10), new Point(29, 6, -12),
                         new Point(29, 0, -12),
                         COLOR_ESCADA);
    building.addRectFace(new Point(-5, 0, -10), new Point(-5, 6, -10), new Point(-5, 6, -12),
                         new Point(-5, 0, -12),
                         COLOR_ESCADA);

    building.addRectFace(new Point(-5, 0, -12), new Point(-5, 8, -12), new Point(-5, 8, -14.9),
                         new Point(-5, 0, -14.9),
                         COLOR_PILLARS);
    building.addRectFace(new Point(29, 0, -12), new Point(29, 8, -12), new Point(29, 8, -14.9),
                         new Point(29, 0, -14.9),
                         COLOR_PILLARS);
    //paredes menores
    building.addRectFace(new Point(0.1, 0, -3), new Point(0.1, 14, -3), new Point(0.1, 14, -5.1),
                         new Point(0.1, 0, -5.1),
                         COLOR_PILLARS);
    building.addRectFace(new Point(23.9, 0, -3), new Point(23.9, 14, -3), new Point(23.9, 14, -5.1),
                         new Point(23.9, 0, -5.1),
                         COLOR_PILLARS);
    building.addRectFace(new Point(0.1, 0, -17), new Point(0.1, 14 ,-17), new Point(0.1, 14, -14.9),
                         new Point(0.1, 0, -14.9),
                         COLOR_PILLARS);
    building.addRectFace(new Point(23.9, 0, -17), new Point(23.9, 14, -17), new Point(23.9, 14, -14.9),
                         new Point(23.9, 0, -14.9),
                         COLOR_PILLARS);

    building.addRectFace(new Point(0.1, 8, -14.8), new Point(0.1, 8, -17), new Point(24, 8, -17),
                         new Point(24, 8, -14.8),
                         COLOR_FLOOR_INTERIOR);
    building.addRectFace(new Point(0.1, 8, -3), new Point(0.1, 8, -5.2), new Point(15, 8, -5.2),
                         new Point(15, 8, -3),
                         COLOR_FLOOR_INTERIOR);

    //paredes divisórias esquerda
    building.addCube(new Point(0.1, 0, -5.1), 0.1, 8, -3, COLOR_PILLARS);
    building.addCube(new Point(0.1, 0, -15), 0.1, 8, 2, COLOR_PILLARS);
    building.addCube(new Point(0.1, 7, -8.1), 0.1, 1, -5, COLOR_PILLARS);

    building.addCube(new Point(0.1, 0, -8.1), 0.1, 7, -0.3, COLOR_DETAILS);
    building.addCube(new Point(0.1, 0, -13), 0.1, 7, 0.3, COLOR_DETAILS);
    building.addCube(new Point(0.1, 7, -8.1), 0.1, -0.3, -4.9, COLOR_DETAILS);

    //paredes divisórias direita
    building.addCube(new Point(24, 0, -5.1), 0.1, 8, -3, COLOR_PILLARS);
    building.addCube(new Point(24, 0, -15), 0.1, 8, 2, COLOR_PILLARS);
    building.addCube(new Point(24, 7, -8.1), 0.1, 1, -5, COLOR_PILLARS);

    building.addCube(new Point(24, 0, -8.1), 0.1, 7, -0.3, COLOR_DETAILS);
    building.addCube(new Point(24, 0, -13), 0.1, 7, 0.3, COLOR_DETAILS);
    building.addCube(new Point(24, 7, -8.1), 0.1, -0.3, -4.9, COLOR_DETAILS);
}

//escadas do interior
void ladder(){
    //escada da porta
    building.addCube(new Point(10, 0, -16.3), 4, 0.8, 5, COLOR_FLOOR_INTERIOR);
    building.addCube(new Point(10, 0, -16.6), 4, 0.6, 5.6, COLOR_FLOOR_INTERIOR);
    building.addCube(new Point(10, 0, -17), 4, 0.4, 6.2,COLOR_FLOOR_INTERIOR);


    //escada grande
    building.addCube(new Point(14, 1, -3.1), 1, 0.5, -3, COLOR_CHAO_ESCADA);
    building.addCube(new Point(15, 1.5, -3.1), 1, 0.5, -3, COLOR_CHAO_ESCADA);
    building.addCube(new Point(16, 2, -3.1), 1, 0.5, -3, COLOR_CHAO_ESCADA);
    building.addCube(new Point(17, 2.5, -3.1), 1, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(18, 3, -3.1), 1, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(19, 3.5, -3.1), 1, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(20, 4, -3.1), 1, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 4.5, -3.1), 1, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 5, -3.1), 2.9, 0.5, -3,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 5.5, -4.6), 2.9, 0.5, -1,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 6, -5.6), 2.9, 0.5, -1,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 6.5, -6.6), 2.9, 0.5, -1,COLOR_CHAO_ESCADA);
    building.addCube(new Point(21, 7, -7.6), 2.9, 1, -0.5,COLOR_CHAO_ESCADA);
}

void coluns(){
    building.addCylinder(new Point(6, 1, -8.5), 0.5, 0.5, 7, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(6, 1, -13.5), 0.5, 0.5, 7, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(18, 1, -8.5), 0.5, 0.5, 7, -90.0f, axisX, COLOR_DETAILS);
    building.addCylinder(new Point(18, 1, -13.5), 0.5, 0.5, 7, -90.0f, axisX, COLOR_DETAILS);
}

void loadTexture(char* filename, int width, int height, int nrChannels) {
  unsigned int texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  unsigned char *data = stbi_load(filename, &width, &height, &nrChannels, 0);
  if (data) {
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      //glGenerateMipmap(GL_TEXTURE_2D);

  } else {
      printf("Failed to load texture");
  }

  stbi_image_free(data);

}

vector< Vertex > loadObjects(string pathfile, int scale, int px, int py, int pz) {

  ifstream ifile( pathfile );

  if (!ifile) {
    printf("Could not open the file %s \n", pathfile.c_str());
  }

  vector< Vertex > verts;
  vector< vec4 > positions( 1, vec4( 0, 0, 0, 0 ));
  vector< vec3 > texcoords( 1, vec3( 0, 0, 0 ));
  vector< vec3 > normals( 1, vec3( 0, 0, 0 ));
  string lineStr;

  while(getline( ifile, lineStr)) {
      istringstream lineSS( lineStr );
      string lineType;
      lineSS >> lineType;

      // vertex
      if(lineType == "v") {
          float x = 0, y = 0, z = 0, w = 1;
          lineSS >> x >> y >> z >> w;
          positions.push_back(vec4(x*scale + px, y*scale + py, z*scale + pz, w ));
      }

      // texture
      if(lineType == "vt") {
          float u = 0, v = 0, w = 0;
          lineSS >> u >> v >> w;
          texcoords.push_back(vec3(u, v, w ));
      }

      // normal
      if(lineType == "vn") {
          float i = 0, j = 0, k = 0;
          lineSS >> i >> j >> k;
          normals.push_back(normalize(vec3( i, j, k )));
      }

      // polygon
      if(lineType == "f") {
          vector< VertRef > refs;
          string refStr;
          while( lineSS >> refStr ) {
              istringstream ref( refStr );
              string vStr, vtStr, vnStr;
              getline( ref, vStr, '/' );
              getline( ref, vtStr, '/' );
              getline( ref, vnStr, '/' );
              int v = atoi( vStr.c_str() );
              int vt = atoi( vtStr.c_str() );
              int vn = atoi( vnStr.c_str() );
              v  = (  v >= 0 ?  v : positions.size() +  v );
              vt = ( vt >= 0 ? vt : texcoords.size() + vt );
              vn = ( vn >= 0 ? vn : normals.size()   + vn );
              refs.push_back( VertRef( v, vt, vn ) );
          }

          if( refs.size() < 3 ) {
              // error, skip
              continue;
          }

          // triangulate, assuming n>3-gons are convex and coplanar
          VertRef* p[3] = { &refs[0], NULL, NULL };
          for(size_t i = 1; i+1 < refs.size(); ++i)  {
              p[1] = &refs[i+0];
              p[2] = &refs[i+1];

              // http://www.opengl.org/wiki/Calculating_a_Surface_Normal
              vec3 U( positions[ p[1]->v ] - positions[ p[0]->v ] );
              vec3 V( positions[ p[2]->v ] - positions[ p[0]->v ] );
              vec3 faceNormal = normalize( cross( U, V ) );

              for(size_t j = 0; j < 3; ++j) {
                  Vertex vert;
                  vert.position = vec3( positions[ p[j]->v ] );
                  vert.texcoord = vec2( texcoords[ p[j]->vt ] );
                  vert.normal   = ( p[j]->vn != 0 ? normals[ p[j]->vn ] : faceNormal );
                  verts.push_back( vert );
              }
          }
      }
  }
  return verts;
}

//Carrega os objetos importados
vector <Vertex> estatua = loadObjects( "/home/larissa/Área de Trabalho/Misa/src/imported/LibertStatue.obj", 5, 12.9, 1, 26);

void displayObjectImported(vector <Vertex> model, int r, int g, int b) {

  glPushMatrix();
      // object
      glColor3ub( r, g, b );
      glEnableClientState( GL_VERTEX_ARRAY );
      glEnableClientState( GL_TEXTURE_COORD_ARRAY );
      glEnableClientState( GL_NORMAL_ARRAY );
      glVertexPointer( 3, GL_FLOAT, sizeof(Vertex), &model[0].position );
      glTexCoordPointer( 2, GL_FLOAT, sizeof(Vertex), &model[0].texcoord );
      glNormalPointer( GL_FLOAT, sizeof(Vertex), &model[0].normal );
      glDrawArrays( GL_TRIANGLES, 0, model.size() );
      glDisableClientState( GL_VERTEX_ARRAY );
      glDisableClientState( GL_TEXTURE_COORD_ARRAY );
      glDisableClientState( GL_NORMAL_ARRAY );

  glPopMatrix();

}

void table(){
    //mesas
    building.addCube(new Point(9.5, 1, -6), -1.5, 1, 0.5, COLOR_CHAO_ESCADA);

    building.addCube(new Point(6, 1, -10.75), 0.7, 0.4, 1.5, COLOR_BLACK);
    building.addCube(new Point(6, 1, -12.5), 0.7, 0.4, 1.5, COLOR_BLACK);

    building.addCube(new Point(18, 1, -10.75), 0.7, 0.4, 1.5, COLOR_BLACK);
    building.addCube(new Point(18, 1, -12.5), 0.7, 0.4, 1.5, COLOR_BLACK);

    building.addCube(new Point(20, 1, -8.5), 2, 0.4, 2.5, COLOR_BLACK);
    building.addCube(new Point(2, 1, -8.5), 2, 0.4, 2.5, COLOR_BLACK);

    building.addCube(new Point(28.9, 1, -12.5), -0.4, 1, -0.5, COLOR_BLACK);
    building.addCube(new Point(-4.9, 1, -12.5), 0.4, 1, -0.5, COLOR_BLACK);

    //mesas marrons
    building.addCube(new Point(-4, 1.5, -5.1), 1.5, 0.2, -1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-4, 1, -5.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-4, 1, -6), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-2.6, 1, -5.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-2.6, 1, -6), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);

    building.addCube(new Point(28, 1.5, -5.1), -1.5, 0.2, -1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(27.9, 1, -5.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(27.9, 1, -6), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(26.5, 1, -5.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(26.5, 1, -6), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);


    building.addCube(new Point(-7, 1.5, -7), -1.5, 0.2, -1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-7.1, 1, -7), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-7.1, 1, -7.9), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-8.5, 1, -7), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(-8.5, 1, -7.9), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);

    building.addCube(new Point(31, 1.5, -7.1), 1.5, 0.2, -1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(31, 1, -7.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(31, 1, -8), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(32.4, 1, -7.1), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);
    building.addCube(new Point(32.4, 1, -8), 0.1, 0.5, -0.1, COLOR_CHAO_ESCADA);


    //mesa do auditorio
    building.addCube(new Point(8, 8, -12), 9, 1.5, -2, COLOR_MESA);


}

//cadeiras
void chair(){

    //cadeira palestrantes
    building.addCube(new Point(10, 8.75, -15.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(10, 8.75, -15.5), 1.1, 1.5, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(12, 8.75, -15.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(12, 8.75, -15.5), 1.1, 2, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(13, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(13, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(14, 8.75, -15.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(14, 8.75, -15.5), 1.1, 1.5, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -15.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -15.5), 0.1, 0.75, -0.1, COLOR_ESCADA);


    //cadeiras fila 1
    building.addCube(new Point(4, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(4, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(4, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(4, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(4, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(4, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(4, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(4, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(4, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(5, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);


    //cadeiras fila 2
    building.addCube(new Point(6, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(6, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(6, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(6, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(6, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(6, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(6, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(6, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(6, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    //fileira 3
    building.addCube(new Point(9, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(9, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(9, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(9, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(9, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(9, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(9, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(9, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(9, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(10, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    //fileira 4
    building.addCube(new Point(11, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(11, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(11, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(11, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(11, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(11, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(11, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(11, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(11, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(12, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    //fileira 5
    building.addCube(new Point(14, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(14, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(14, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(14, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(14, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(14, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(14, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(14, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(14, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(15, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    //fileira 6
    building.addCube(new Point(16, 8.75, -7.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(16, 8.75, -7.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -7.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -7.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(16, 8.75, -8.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(16, 8.75, -8.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -8.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -8.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(16, 8.75, -9.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(16, 8.75, -9.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -9.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -9.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    building.addCube(new Point(16, 8.75, -10.1), 1.1, 0.1, -0.5, COLOR_ESCADA);
    building.addCube(new Point(16, 8.75, -10.1), 1.1, 1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(16, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -10.1), 0.1, 0.75, -0.1, COLOR_ESCADA);
    building.addCube(new Point(17, 8, -10.5), 0.1, 0.75, -0.1, COLOR_ESCADA);

    //cadeira vigia
    building.addCube(new Point(7, 1.5, -6.1), 0.7, 0.1, -0.6, COLOR_ESCADA);
    building.addCube(new Point(7, 1.5, -6.1), 0.7, 1.1, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 1, -6.1), 0.1, 0.6, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7, 1, -6.5), 0.1, 0.6, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7.6, 1, -6.1), 0.1, 0.6, -0.1, COLOR_ESCADA);
    building.addCube(new Point(7.6, 1, -6.5), 0.1, 0.6, -0.1, COLOR_ESCADA);

}

void interior(){

    walls();
    ladder();
    coluns();
    table();
    interiorWindows();
    chair();
}

void exterior(){

    exteriorWalls();
    outsideWindows();
    specialWindows();
    courtyard();
    hill();
    ceiling();
    balcony();
}

void initTexture () {

	image_t temp_image;
	glEnable ( GL_TEXTURE_2D );

	glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
	glGenTextures (1, texture_id);  // 1 = uma textura;

  texture_id[TELHADO] = 1001;
  texture_id[PAREDE] = 1002;

	glBindTexture ( GL_TEXTURE_2D, texture_id[TELHADO] );
	tgaLoad("C:/Develop/DevCG/misa-3d/Misa/src/imported/texture/telhado.tga", &temp_image, TGA_FREE | TGA_LOW_QUALITY );

  glBindTexture ( GL_TEXTURE_2D, texture_id[PAREDE] );
  tgaLoad("imported/texture/telhado.tga", &temp_image, TGA_FREE | TGA_LOW_QUALITY );
}

void init() {
    // sky color
    glClearColor(0.0, 0.7, 1.0, 1.0);

    exterior();
    interior();

}

void drawCube(Point *p, float width, float height, float depth, Point *rotationPoint, float angle, int rotationAxis[3],
              Color *color) {
    glPushMatrix();
        glColor3f(GLfloat(color->R), GLfloat(color->G), GLfloat(color->B));
        glTranslatef(GLfloat(p->x), GLfloat(p->y), GLfloat(p->z));
        glRotatef(angle, rotationAxis[0], rotationAxis[1], rotationAxis[2]);
        glTranslatef(GLfloat(rotationPoint->x), GLfloat(rotationPoint->y), GLfloat(rotationPoint->z));
        glScalef(width, height, depth);
        glutSolidCube(1.0);
    glPopMatrix();
}

void drawCylinder(Point *pos, float base, float top, float height, float rotAngle, int rotationAxis[3], Color *color) {
    glPushMatrix();
        glTranslatef(GLfloat(pos->x), GLfloat(pos->y), GLfloat(pos->z));
        glColor3f(GLfloat(color->R), GLfloat(color->G), GLfloat(color->B));
        glRotatef(rotAngle, rotationAxis[0], rotationAxis[1], rotationAxis[2]);
        gluCylinder(gluNewQuadric(), base, top, height, 50, 50);
    glPopMatrix();
}

void drawModel(Model model) {
    //desenha uma face

    for (auto &face : model.faces) {

        if (face->local.c_str() != "") {
          double texCoord[4][2] = {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}};
          int count = 0;

          printf("%s", face->local.c_str());


          glBindTexture ( GL_TEXTURE_2D, face->nro_texture);
        	glBegin ( GL_QUADS );
              for (auto &point : face->points) {
                  glTexCoord2f(texCoord[count][0], texCoord[count][1]); glVertex3f(GLfloat(point->x), GLfloat(point->y), GLfloat(point->z));
                  count += count + 1;
              }
          glEnd();

        } else {

        glColor3f(GLfloat(face->color->R), GLfloat(face->color->G), GLfloat(face->color->B));
        glBegin(GL_POLYGON);
            for (auto &point : face->points) {
                glVertex3f(GLfloat(point->x), GLfloat(point->y), GLfloat(point->z));
            }
        glEnd();
      }
    }

    for (auto &cube : model.cubes) {
        //desenha um cubo
        glColor3f(GLfloat(cube->color->R), GLfloat(cube->color->G), GLfloat(cube->color->B));
        drawCube(cube->position, cube->width, cube->height, cube->depth, cube->rotationPoint, cube->rotationAngle,
                 cube->rotationAxis, cube->color);
    }

    for (auto &cylinder : model.cylinders) {
        drawCylinder(cylinder->position, cylinder->base, cylinder->top, cylinder->height, cylinder->rotationAngle,
                     cylinder->rotationAxis, cylinder->color);
    }
}

void drawDoor(){
    int orientation[] = {0, 1, 0};
    // Left
    drawCube(new Point(10, 0, -17.1), 3, 7, -0.2, new Point(0.5, 1.5, 0.05), door_angle1, orientation, COLOR_ESCADA);

    // Right
    orientation[1] = -1;
    drawCube(new Point(14, 0, -17.1), 3, 7, -0.2, new Point(-0.5, 1.5, 0.05), door_angle2, orientation, COLOR_ESCADA);
}

void drawBuilding() {
    glPushMatrix();
        glTranslatef(building.x, building.y, building.z);
		drawModel(building);
        drawDoor();

    glPopMatrix();
}

void changeSize(int w, int h) {

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if (h == 0) h = 1;
    double ratio = w * 1.0 / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

    // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(45.0f, ratio, 0.1f, 100.0f);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}

void keyboardHandler(unsigned char key, int x, int y) {
    keyPressed[key] = true;

    if (key == 27)
        exit(0);
}

void keyboardUpHandler(unsigned char key, int x, int y) {
    keyPressed[key] = false;
    if (key == 'q' && cam->speed > 0.03) {
        cam->speed -= 0.01;
    }
    if (key == 'e' && cam->speed < 0.2) {
        cam->speed += 0.01;
    }
}

void updateCamera() {
    if (keyPressed['w']) {
        cam->moveForward();
    }
    if (keyPressed['s']) {
        cam->moveBackward();
    }
    if (keyPressed['a']) {
        cam->moveLeft();
    }
    if (keyPressed['d']) {
        cam->moveRight();
    }
    if (keyPressed[' ']) {
        cam->moveUp();
    }
    if (specialKeyPressed[GLUT_KEY_SHIFT]) {
        cam->moveDown();
    }
    if (specialKeyPressed[GLUT_KEY_LEFT]) {
        cam->lookLeft();
    }
    if (specialKeyPressed[GLUT_KEY_RIGHT]) {
        cam->lookRight();
    }
    if (specialKeyPressed[GLUT_KEY_UP]) {
        cam->lookUp();
    }
    if (specialKeyPressed[GLUT_KEY_DOWN]) {
        cam->lookDown();
    }

}

void updateDoor() {
    if (keyPressed['o'])
        if (door_angle1 <= 118.0f)
            door_angle1 += 2.0f;
    if(keyPressed['O'])
        if(door_angle2 <= 118.0f)
            door_angle2 += 2.0f;

    if (keyPressed['c'])
        if (door_angle1 >= 2.0f)
            door_angle1 -= 2.0f;

    if (keyPressed['C'])
        if (door_angle2 >= 2.0f)
            door_angle2 -= 2.0f;

    if (keyPressed['m'])
        if (door_angle2 >= 2.0f and door_angle1 >= 2.0f){
            door_angle2 -= 2.0f;
            door_angle1 -=2.0f;
        }

    if (keyPressed['M'])
        if (door_angle2 <= 118.0f and door_angle1 <= 118.0f){
            door_angle2 += 2.0f;
            door_angle1 +=2.0f;

        }

}

void update() {
    updateCamera();
    updateDoor();
}

// The one and only moon.
static SunMoon moon;

void renderScene(int) {
    update();
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Reset transformations
    glLoadIdentity();

    // Coloca a camera
    gluLookAt(cam->position.x, cam->position.y, cam->position.z,
              cam->position.x + cam->direction.x, cam->position.y + cam->direction.y,
              cam->position.z + cam->direction.z,
              0.0f, 1.0f, 0.0f);

    // Desenha chão
    glColor3f(0.411, 0.411, 0.411);
    glBegin(GL_QUADS);
        glVertex3f(-100.0f, 0.0f, -100.0f);
        glVertex3f(-100.0f, 0.0f, 100.0f);
        glVertex3f(100.0f, 0.0f, 100.0f);
        glVertex3f(100.0f, 0.0f, -100.0f);
    glEnd();



        glColor3d(1,0,0);

        glPushMatrix();
            glTranslated(111.0, 222.0,6.0);
            glutSolidSphere(100000,50,50);
        glPopMatrix();

        glPushMatrix();
            glTranslated(0.0,-1.2,-6);
            glutWireSphere(1,16,16);
        glPopMatrix();

    drawBuilding();

    //Mostra os objetos importados
    //displayObjectImported(estatua, 128, 120, 112);

//larissa olha aqui
    // glEnable(GL_DEPTH_TEST);
    // GLfloat yellow[] = {1.0, 1.0, 0.5, 1.0};
    // glLightfv(GL_LIGHT0, GL_DIFFUSE, yellow);
    // glEnable(GL_LIGHTING);
    // glEnable(GL_LIGHT0);
    // moon.create(0);



    glFlush();
    glutSwapBuffers();
    glutTimerFunc(redisplay_interval, renderScene, 0);

}

void renderScene() {
    renderScene(0);
}

void specialFuncHandler(int key, int x, int y) {
    specialKeyPressed[key] = true;
}

void specialFuncUpHandler(int key, int x, int y) {
    specialKeyPressed[key] = false;
}

void print(){
    printf("\n\n------------------Comandos-----------------");
    printf("\n   w - caminha para frente");
    printf("\n   s - caminha para tŕas");
    printf("\n   d - caminha para direita");
    printf("\n   a - caminha para esquerda");
    printf("\n   o - abre porta direita com relação ao exterior");
    printf("\n   O - abre porta esquerda com relação ao exterior");
    printf("\n   c - fecha porta direita com relação ao exterior");
    printf("\n   C - fecha porta esquerda com relação ao exterior");
    printf("\n   m - fecha ambas as portas ");
    printf("\n   M - abre ambas as portas ");
    printf("\n   e - aumenta velocidade de caminhada ");
    printf("\n   q - diminui velocidade de caminhada \n");

}

int main(int argc, char **argv) {

    // inicia Glut e cria janela
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(50, 50);
    glutInitWindowSize(800, 600);
    glutCreateWindow("CH - Museu da Imagem e do Som de Alagoas");

    init();
    initTexture();
    print();

    //callbacks
    glutDisplayFunc(renderScene);

    //glutTimerFunc(100, timer, 100);

    glutReshapeFunc(changeSize);
    glutIdleFunc(update);
    glutTimerFunc(redisplay_interval, renderScene, 0);
    glutKeyboardFunc(keyboardHandler);
    glutKeyboardUpFunc(keyboardUpHandler);
    glutSpecialFunc(specialFuncHandler);
    glutSpecialUpFunc(specialFuncUpHandler);


    // inicia OpenGL
    glEnable(GL_DEPTH_TEST);

    // mantem GLUT em loop
    glutMainLoop();

    return 0;
}
