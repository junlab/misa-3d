#include "Point.h"
#include "Color.h"
#include "Face.h"
#include <GL/glut.h>


Face::Face(int vertices, Point* points[], Color* color) {
    for (int i = 0; i < vertices; i++) {
        this->points.push_back(points[i]);
    }
    this->color = color;
    this->nro_texture = 0;
    this->local = "";
  }

Face::Face(int vertices, Point* points[], GLuint nro_texture, string local) {

  for (int i = 0; i < vertices; i++) {
      this->points.push_back(points[i]);
  }

  this->local = local;

  // Queria que isso aparecesse la no drawModel
  // if (local == "F") {
  //   this->texCoord.push_back(Point(0.0d, 0.0d, 0.0d));
  //   this->texCoord.push_back(Point(1.0d, 0.0d, 0.0d));
  //   this->texCoord.push_back(Point(1.0d, 1.0d, 0.0d));
  //   this->texCoord.push_back(Point(0.0d, 1.0d, 0.0d));
  //
  // } else if (local == "B") {
  //   this->texCoord.push_back(Point(1.0d, 0.0d, 0.0d));
  //   this->texCoord.push_back(Point(1.0d, 1.0d, 0.0d));
  //   this->texCoord.push_back(Point(0.0d, 1.0d, 0.0d));
  //   this->texCoord.push_back(Point(0.0d, 0.0d, 0.0d));
  // }

  this->nro_texture = nro_texture;

}

void Face::setColor(Color *color) {
    this->color = color;
}
