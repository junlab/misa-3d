
#include "SunMoon.h"
#include <GL/glut.h>

SunMoon::SunMoon() {
  int displayListId;
}

void SunMoon::create(int sm) {
  displayListId = glGenLists(1);
  glNewList(displayListId, GL_COMPILE);
  GLfloat direction[] = {18, 2, 18, 0.0};
  glLightfv(GL_LIGHT0, GL_POSITION, direction);
  glutSolidSphere(20, 25, 25);
  glEndList();
}

void SunMoon::draw() {
  glCallList(displayListId);
}
