cmake_minimum_required(VERSION 3.4)
project(misa)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_PREFIX_PATH "C:/Program Files (x86)/mingw64")

#O comando file(GLOB...) nos permite usar wildcards para simplificar a adição de vários arquivos:
file(GLOB FONTES "src/*.cpp")

#usa FONTES para compilar o exeutável
add_executable(misa ${FONTES})

#Adicionando os cabeçalhos, no projeto
include_directories(includes)
include_directories("C:/Program Files (x86)/mingw64/include")

target_link_libraries(misa -lglu32 -lopengl32 -lfreeglut)
#windows -lglu32 -lopengl32 -lfreeglut 
#linux -lglut -lGL -lGLU
