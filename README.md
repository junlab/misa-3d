Valor 3 Pontos <br /><br />

1 - Data da Entrega das Especificações 2 e 3: 13/02<br />

2 - Definir um Centro Histórico que será usado no projeto de Modelagem;<br />

3 - Enviar para mim qual é o Local, fotos do local e a Dupla;
<br /><br />

###############################
<br />
- Data da Entrega da primeira versão: 11/03
<br />
- Nesta atividade a DUPLA deverá modelar um Centro Histórico (CH) usando o OpenGL.
<br />
- A modelagem deve conter no mínimo: 
<br />
    A fachada externa; <br />
    O interior do CH.; <br />
    5 objetos do CH; <br />
    1 Porta; <br />
    O usuário irá navegar com o teclado/mouse na modelagem permitindo ao usuário visualizar a parte externa e interna do CH; <br />
    A porta do CH deverá ser aberta ou fechada através da ação de um  botão criado em uma interface gráfica. <br />

- Se apoiem nos materiais:  http://www.ceset.unicamp.br/~magic/opengl/transformacoes.html <br />
- Leia a explicação de como realizar a passagem dos objetos wire em  sólidos no final do capítulo;
  http://lab.bachem-it.com/opengl/redbook/<br />
- http://www.glprogramming.com/red/ <br />
- use o livro OpenGL Superbible que passei para vcs na sala de aula; <br />
- Valor da Atividade 3 pontos na nota B1
- Usem a criatividade e bom trabalho !!!<br />